/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Arthi
 */
public class UpdateDatabase {
    public static void main(String[] args) {
Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        //connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connected");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Update
        String sql = "UPDATE store SET STO_Name = ? WHERE STO_ID = ? ";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(2, 5);
            stmt.setString(1, "Momotaro");
            int status = stmt.executeUpdate();
            //ResultSet key = stmt.getGeneratedKeys();
            //key.next();
            //System.out.println(""+key.getInt(1));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //close database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
